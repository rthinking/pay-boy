package com.ndood.core.properties;

/**
 * 浏览器相关安全配置
 * @author ndood
 */
public class BrowserProperties {
	
	/**
	 * 默认登录页
	 */
	private String signInPage = "/default-signIn.html";
	
	/**
	 * 默认注册页
	 */
	private String signUpPage = "/default-signUp.html";
	
	/**
	 * 退出登录默认跳转html，如果配置了就跳转html，否则返回json
	 */
	private String signOutUrl = "";

	/**
	 * 记住我时间
	 */
	private int rememberMeSeconds = 3600;
	
	/**
	 * 默认登录返回类型
	 */
	private LoginResponseType signInResponseType = LoginResponseType.JSON; 
	
	/**
	 * session相关配置
	 */
	private SessionProperties session = new SessionProperties();
	
	public String getSignInPage() {
		return signInPage;
	}

	public void setSignInPage(String signInPage) {
		this.signInPage = signInPage;
	}

	public LoginResponseType getSignInResponseType() {
		return signInResponseType;
	}

	public void setSignInResponseType(LoginResponseType signInResponseType) {
		this.signInResponseType = signInResponseType;
	}

	public int getRememberMeSeconds() {
		return rememberMeSeconds;
	}

	public void setRememberMeSeconds(int rememberMeSeconds) {
		this.rememberMeSeconds = rememberMeSeconds;
	}

	public String getSignUpPage() {
		return signUpPage;
	}

	public void setSignUpPage(String signUpPage) {
		this.signUpPage = signUpPage;
	}

	public SessionProperties getSession() {
		return session;
	}

	public void setSession(SessionProperties session) {
		this.session = session;
	}

	public String getSignOutUrl() {
		return signOutUrl;
	}

	public void setSignOutUrl(String signOutUrl) {
		this.signOutUrl = signOutUrl;
	}
}
