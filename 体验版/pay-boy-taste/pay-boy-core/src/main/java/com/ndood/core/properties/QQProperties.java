package com.ndood.core.properties;

/**
 * 添加QQ属性配置类
 */
public class QQProperties {
	
	private String providerId = "qq"; // 服务商标识
	
	private String appId = ""; 
	
	private String appSecret = "";

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	
	
}