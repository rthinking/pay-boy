package com.ndood.admin.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ndood.admin.pojo.comm.vo.AdminResultVo;
import com.ndood.admin.pojo.system.RegionPo;
import com.ndood.admin.pojo.system.dto.RegionDto;
import com.ndood.admin.service.system.SystemRegionService;

/**
 * 地区控制器类
 * @author ndood
 */
@Controller
public class SystemRegionController {
	
	@Autowired
	private SystemRegionService systemRegionService;

	/**
	 * 显示地区页
	 */
	@GetMapping("/system/region")
	public String toRegionPage(){
		return "system/region/region_page";
	}
	
	/**
	 * 显示添加对话框
	 */
	@GetMapping("/system/region/add")
	public String toAddRegion(Integer parentId,String parentName, Model model){
		if(!StringUtils.isEmpty(parentId)&&!StringUtils.isEmpty(parentName)){
			model.addAttribute("parentId", parentId);
			model.addAttribute("parentName", parentName);
		}
		return "system/region/region_add";
	}
	
	@PostMapping("/system/region/add")
	@ResponseBody
	public AdminResultVo addRegion(@RequestBody RegionDto region) throws Exception{
		RegionDto obj = systemRegionService.addRegion(region);
		return AdminResultVo.ok().setData(obj).setMsg("添加地区成功！");
	}
	
	@GetMapping("/system/region/treeview")
	@ResponseBody
	public List<RegionDto> getRegionTreeview(String keywords, Integer parentId) throws Exception{
		List<RegionDto> regionList = systemRegionService.getRegionList(keywords, parentId);
		return regionList;
	}
	
	@PostMapping("/system/region/province")
	@ResponseBody
	public AdminResultVo getProvince(String provinceId) throws Exception{
		List<RegionDto> regionList = systemRegionService.getProvinceList(provinceId);
		return AdminResultVo.ok().setData(regionList).setMsg("获取地区数据成功！");
	}
	
	@PostMapping("/system/region/delete")
	@ResponseBody
	public AdminResultVo deleteRegion(Integer id){
		Integer[] ids = new Integer[]{id};
		systemRegionService.batchDeleteRegion(ids);
		return AdminResultVo.ok().setMsg("删除地区成功！");
	}
	
	@PostMapping("/system/region/batch_delete")
	@ResponseBody
	public AdminResultVo batchDeleteRegion(@RequestParam("ids[]") Integer[] ids){
		systemRegionService.batchDeleteRegion(ids);
		return AdminResultVo.ok().setMsg("批量删除地区成功！");
	}
	
	/**
	 * 显示修改对话框
	 */
	@GetMapping("/system/region/update")
	public String toUpdateRegion(Integer id, Model model) throws Exception{
		RegionPo region = systemRegionService.getRegion(id);
		model.addAttribute("region", region);
		return "system/region/region_update";
	}
	
	/**
	 * 修改地区
	 */
	@PostMapping("/system/region/update")
	@ResponseBody
	public AdminResultVo updateRegion(@RequestBody RegionDto region) throws Exception{
		RegionDto obj = systemRegionService.updateRegion(region);
		return AdminResultVo.ok().setData(obj).setMsg("修改地区成功！");
	}
}
