package com.ndood.admin.core.validator;

/**
 * 自定义validator校验器
 * @author ndood
 * 不用写@Component 因为实现了ConstraintValidator，spring会自动引用
 */
/*public class MyConstraintValidator implements ConstraintValidator<MyConstraint, Object>{

	// 为了测试自定义validator注解写的service
	@Autowired
	private HelloService helloService;
	
	@Override
	public void initialize(MyConstraint arg0) {
		System.out.println("my validator init");
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		helloService.greeting("tom");
		System.out.println(value);
		return false;
	}

}
*/