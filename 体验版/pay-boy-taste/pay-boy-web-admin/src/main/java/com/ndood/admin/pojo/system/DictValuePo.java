package com.ndood.admin.pojo.system;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 字典值bean
 * @author ndood
 */
@Entity
@Table(name="t_dict_value")
@Cacheable(true)
@Getter @Setter
public class DictValuePo implements Serializable{
	private static final long serialVersionUID = 4018789970118561996L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="`code`", length=32, nullable=false)
	private String code;
	
	@Column(name="`desc`", length=32)
	private String desc;
	
}
