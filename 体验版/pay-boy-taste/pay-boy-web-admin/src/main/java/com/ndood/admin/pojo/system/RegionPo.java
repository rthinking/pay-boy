package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/*
 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
CascadeType.REMOVE
Cascade remove operation，级联删除操作。
删除当前实体时，与它有映射关系的实体也会跟着被删除。
CascadeType.MERGE
Cascade merge operation，级联更新（合并）操作。
当Student中的数据改变，会相应地更新Course中的数据。
CascadeType.DETACH
Cascade detach operation，级联脱管/游离操作。
如果你要删除一个实体，但是它有外键无法删除，你就需要这个级联权限了。它会撤销所有相关的外键关联。
CascadeType.REFRESH
Cascade refresh operation，级联刷新操作。
假设场景 有一个订单,订单里面关联了许多商品,这个订单可以被很多人操作,那么这个时候A对此订单和关联的商品进行了修改,与此同时,B也进行了相同的操作,但是B先一步比A保存了数据,那么当A保存数据的时候,就需要先刷新订单信息及关联的商品信息后,再将订单及商品保存。(来自良心会痛的评论)
CascadeType.ALL
Cascade all operations，清晰明确，拥有以上所有级联操作权限。
*/
/**
 * 地区bean
 * @author ndood
 */
@Entity
@Table(name="t_region")
@Cacheable(true)
@Getter @Setter
public class RegionPo implements Serializable{
	private static final long serialVersionUID = 9001330557718325094L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 原始ID
	 */
	private Integer sid;
	
	/**
	 * 简称
	 */
	private String shortname;

	/**
	 * 地区名
	 */
	private String name;
	
	/**
	 * 拼音
	 */
	private String pinyin;

	/**
	 * 纬度
	 */
	private Double lng;

	/**
	 * 经度
	 */
	private Double lat;
	
	/**
	 * 是否是叶子节点 0 不是 1 是
	 */
	@Column(name="is_leaf")
	private Integer isLeaf;
	
	/**
	 * 父地区 对于层级关系的用lazy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private RegionPo parent;
	
	/**
	 * 子地区
	 */
	@OneToMany(cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private List<RegionPo> children;

}
