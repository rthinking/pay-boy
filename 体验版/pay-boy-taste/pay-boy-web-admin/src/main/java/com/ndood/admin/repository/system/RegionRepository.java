package com.ndood.admin.repository.system;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.RegionPo;

/**
 * 地区dao
 * @author ndood
 */
public interface RegionRepository extends JpaRepository<RegionPo, Integer>{
	
	List<RegionPo> findAll(Specification<RegionPo> specification);
	
	RegionPo findBySid(Integer sid);
}
