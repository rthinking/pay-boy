package com.ndood.admin.service.system;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.dto.UserDto;
import com.ndood.admin.pojo.system.query.UserQuery;

/**
 * 用户管理业务接口
 * @author ndood
 */
public interface SystemUserService {

	/**
	 * 添加用户
	 */
	String addUser(UserDto user) throws Exception;

	/**
	 * 批量删除用户
	 */
	void batchDeleteUser(Integer[] ids);

	/**
	 * 更新用户
	 */
	void updateUser(UserDto user) throws Exception;

	/**
	 * 获取单个用户
	 */
	UserDto getUser(Integer id) throws Exception;

	/**
	 * 获取用户列表
	 */
	DataTableDto pageUserList(UserQuery query) throws Exception;

}
