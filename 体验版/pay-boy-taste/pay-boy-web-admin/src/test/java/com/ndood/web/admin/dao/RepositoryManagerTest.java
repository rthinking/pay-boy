package com.ndood.web.admin.dao;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ndood.admin.repository.system.UserRepository;

/**
 * 新建测试类
 * https://www.objectdb.com/java/jpa/query/jpql/from
 * https://www.w3cschool.cn/java/jpa-query-join-manytoone.html
 * http://docs.jboss.org/hibernate/orm/5.0/manual/en-US/html_single/#queryhql
 * @author ndood
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryManagerTest {
	
	@Autowired
	private UserRepository userDao;
	
	@Test
	@Transactional
	public void testDao() throws Exception{
	}
	
	@Test
	public void testPageDao() throws Exception{
	}
	
	@Test
	public void testPageDao2() throws Exception{
	}

}
